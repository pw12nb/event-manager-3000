package inventtech.eventmanager3000.Helper;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Peter on 9/12/2015.
 *
 * The helper used to initialize the meeting info database
 */
public class MeetingDatabaseHelper extends SQLiteOpenHelper {

    public static final String TABLE_MEETINGINFO = "meetingInfo";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_date = "Date_Of_Meeting";
    public static final String COLUMN_description = "Description";
    public static final String COLUMN_time = "Time";
    public static final String COLUMN_location = "Location";
    public static final String COLUMN_contact = "Contact";
    public static final String COLUMN_notes = "Notes";

    private static final String DATABASE_NAME = "userInfo.db";
    private static final int DATABASE_VERSION = 4;

    // Database creation sql statement
    private static final String DATABASE_CREATE = "create table "+
            TABLE_MEETINGINFO + "(" +
            COLUMN_ID + " integer primary key autoincrement, " +
            COLUMN_date + " varchar(25) not null," +
            COLUMN_description + " varchar(50) not null,"+
            COLUMN_time + " varchar(50) not null,"+
            COLUMN_location + " varchar(50) not null,"+
            COLUMN_contact + " varchar(50) not null,"+
            COLUMN_notes + " varchar(50) not null);";

    public MeetingDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Create the new DB
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(DATABASE_CREATE);
    }

    /**
     * deconstructs the old DB and creates the new one
     * @param db the database to update
     * @param oldVersion the old version number
     * @param newVersion the new version number
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(MeetingDatabaseHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MEETINGINFO);
        onCreate(db);
    }


}
