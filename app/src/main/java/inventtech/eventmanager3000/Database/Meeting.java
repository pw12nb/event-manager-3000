package inventtech.eventmanager3000.Database;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;

/**
 * Created by Peter on 9/12/2015.
 * A Class that contains all the meeting information. This is added to the database or
 * pulled from the database
 */
public class Meeting implements Serializable {
    private long id;
    private Date dateOfMeeting;
    private String description;
    private String time;
    private String location;
    private String contact;
    private String notes;

    /**
     * The meeting object that will be displayed in the list view
     * with data pulled from the database
     * @param dateOfMeeting the date the meeting occurs
     * @param description the title of the meeting
     */
    public Meeting(Date dateOfMeeting, String description)
    {
        this.setDateOfMeeting(dateOfMeeting);
        this.setDescription(description);
    }

    /**
     * Default constructor
     */
    public Meeting(){}

    /**
     * Get the ID of the meeting
     * @return the ID
     */
    public long getId() {
        return id;
    }

    /**
     * Set the ID of the meeting
     * @param id the new ID value
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Get the date of the meeting
     * @return the date of meeting
     */
    public Date getDateOfMeeting() { return dateOfMeeting; }

    /**
     * Set the date of the meeting
     * @param dateOfMeeting the new date of the meeting
     */
    public void setDateOfMeeting(Date dateOfMeeting) { this.dateOfMeeting = dateOfMeeting;  }

    /**
     * Get the description of the meeting
     * @return the description of the meeting
     */
    public String getDescription() { return description; }

    /**
     * Set the description of the meeting
     * @param description the new description of the meeting
     */
    public void setDescription(String description) { this.description = description; }

    /**
     * Get the time of th meeting
     * @return the time of the meeting
     */
    public String getTime() {
        return time;
    }

    /**
     * Set the time of the meeting
     * @param time the new time of the meeting
     */
    public void setTime(String time) {
        this.time = time;
    }

    /**
     * Get the location of the meeting
     * @return the location of the meeting
     */
    public String getLocation() {
        return location;
    }

    /**
     * Set the location of the meeting
     * @param location the new location of the meeting
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * Get the contact name of the meeting
     * @return the contact name
     */
    public String getContact() {
        return contact;
    }

    /**
     * Set the contact name
     * @param contact the new contact name
     */
    public void setContact(String contact) {
        this.contact = contact;
    }

    /**
     * Get the notes associated with the meeting
     * @return the notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * Set the notes associated with the meeting
     * @param notes the new notes
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }
}


