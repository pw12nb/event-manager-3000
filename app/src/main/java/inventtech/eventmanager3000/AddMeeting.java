package inventtech.eventmanager3000;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import java.sql.Date;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import inventtech.eventmanager3000.DTO.MeetingDTO;
import inventtech.eventmanager3000.Database.Meeting;

public class AddMeeting extends AppCompatActivity {

    EditText id;
    EditText description;
    EditText date;
    EditText time;
    EditText location;
    EditText contacts;
    EditText notes;
    MeetingDTO dataTransfer;
    static final int PICK_CONTACT_REQUEST = 1;
    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener newDate;

    /**
     * Sets up the new view
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_meeting);

        //initialize components
        dataTransfer = new MeetingDTO(this);
        id = (EditText)findViewById(R.id.eventID);
        description = (EditText)findViewById(R.id.eventName);
        date = (EditText)findViewById(R.id.eventDate);
        time = (EditText)findViewById(R.id.eventTime);
        location = (EditText)findViewById(R.id.eventLocation);
        contacts = (EditText)findViewById(R.id.eventContact);
        notes = (EditText)findViewById(R.id.eventNotes);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);

        //set the status bar
        Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(this.getResources().getColor(R.color.DarkTeal));

        setSupportActionBar(myToolbar);
        myToolbar.setTitleTextColor(getResources().getColor(R.color.white));

        //launch the date picker if the date has been selected
        date.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //To show current date in the datepicker
                Calendar mcurrentDate = Calendar.getInstance();
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.CANADA);
                try {
                    cal.setTime(sdf.parse(date.getText().toString()));
                } catch (Exception e) {
                }

                DatePickerDialog mDatePicker = new DatePickerDialog(AddMeeting.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        Calendar c = Calendar.getInstance();
                        c.set(selectedyear, selectedmonth, selectedday, 0, 0);
                        date.setText(new Date(c.getTimeInMillis()).toString());
                    }
                }, cal.get(cal.YEAR), cal.get(cal.MONTH), cal.get(cal.DAY_OF_MONTH));
                mDatePicker.setTitle("Select date");
                mDatePicker.show();
            }
        });

        //launch the time picker if the time has been selected
        time.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(AddMeeting.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        time.setText( selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, false);
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });

        //launch the contact intent if the contact has been selected
        contacts.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                try {
                    Intent intent = new Intent(Intent.ACTION_PICK, Uri.parse("content://contacts/people"));
                    startActivityForResult(intent, PICK_CONTACT_REQUEST);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


        //load the passed meeting if we are editing an existing meeting
        Bundle extras = getIntent().getExtras();
        if(extras != null)
        {

            String currentDate = extras.getString("DATE_SELECTED");
            date.setText(currentDate);

            Object obj = extras.get("MEETING_DATA");
            if(obj != null)
            {
                Meeting meeting = (Meeting)obj;
                id.setText(""+meeting.getId());
                description.setText(meeting.getDescription());
                date.setText(meeting.getDateOfMeeting().toString());
                time.setText(meeting.getTime().toString());
                location.setText(meeting.getLocation());
                contacts.setText(meeting.getContact());
                notes.setText(meeting.getNotes());
            }

        }
    }

    /**
     * Add the menu items, customize it depending on if it is being updated or created
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_meeting, menu);
        if(id.getText().toString().equals(""))
        {
            //if making new item, hide the edit functions
            menu.getItem(0).setVisible(false);
            menu.getItem(1).setVisible(false);
            menu.getItem(2).setVisible(false);
        }
        return true;
    }

    /**
     * Gets the contact name from the contact intent
     * @param requestCode
     * @param resultCode
     * @param intent
     */
    @Override
    public void onActivityResult( int requestCode, int resultCode, Intent intent ) {

        super.onActivityResult(requestCode, resultCode, intent);
        if ( requestCode == PICK_CONTACT_REQUEST ) {

            if ( resultCode == RESULT_OK ) {
                Uri pickedContact = intent.getData();
                Cursor c =  getContentResolver().query(pickedContact, null, null, null, null);
                if (c.moveToFirst()) {
                    String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    contacts.setText(name);
                    // TODO Whatever you want to do with the selected contact name.
                }
                // handle the picked phone number in here.
            }
        }
    }

    /**
     * Performs the action based on the menu item selected
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch(item.getItemId()){
            case(R.id.action_save):
                saveMeeting();

            return true;
            case(R.id.action_delete):
                deleteMeeting();
            return true;
            case(R.id.action_next):
                newDate(true);
            return true;
            case(R.id.action_prev):
                newDate(false);
            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Change the date of the object, either forward or backward in time
     * @param forward
     */
    public void newDate(boolean forward)
    {
        if(!id.getText().toString().equals("")) {
            Meeting temp = new Meeting();
            temp.setDescription(description.getText().toString());
            temp.setContact(contacts.getText().toString());
            temp.setLocation(location.getText().toString());
            temp.setNotes(notes.getText().toString());
            temp.setTime(time.getText().toString());
            try {
                temp.setId(Long.valueOf(id.getText().toString()));
            }catch(Exception e)
            {
                Toast.makeText(this,"ID not in the Correct format. Unable to proceed", Toast.LENGTH_SHORT).show();
            }
            try {
                Date newDate = Date.valueOf(date.getText().toString());
                temp.setDateOfMeeting(newDate);
            }catch(Exception e)
            {
                Toast.makeText(this,"Date not in the Correct format. Unable to proceed", Toast.LENGTH_SHORT).show();
            }
            dataTransfer.open();
            dataTransfer.delayMeeting(temp, forward);
            dataTransfer.close();
            Toast.makeText(this,"Meeting Moved to the "+(forward?"Next":"Previous")+" Date", Toast.LENGTH_SHORT).show();
            Intent i = new Intent(this, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finish();
        }
    }

    /**
     * Delete the meeting
     */
    public void deleteMeeting()
    {
        if(!id.getText().toString().equals("")) {
            Meeting temp = new Meeting();
            try {
                temp.setId(Long.valueOf(id.getText().toString()));
            }catch(Exception e)
            {
                Toast.makeText(this,"ID not in the Correct format. Unable to proceed", Toast.LENGTH_SHORT).show();
            }
            dataTransfer.open();
            dataTransfer.deleteMeeting(temp);
            dataTransfer.close();
            Toast.makeText(this,"Meeting Successfully Deleted", Toast.LENGTH_SHORT).show();
            Intent i = new Intent(this, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finish();
        }
    }

    /**
     * Save a new meeting based on the supplied information
     * if the ID is already set, update instead
     */
    public void saveMeeting() {
        Meeting meeting = new Meeting();
        meeting.setDescription(description.getText().toString());
        if(meeting.getDescription().equals(""))
        {
            Toast.makeText(this, "An Event Name is required", Toast.LENGTH_SHORT).show();
            return;
        }
        meeting.setContact(contacts.getText().toString());
        meeting.setLocation(location.getText().toString());
        meeting.setNotes(notes.getText().toString());
        meeting.setTime(time.getText().toString());
        try {
            Date newDate = Date.valueOf(date.getText().toString());
            meeting.setDateOfMeeting(newDate);
        }catch(Exception e)
        {
            Toast.makeText(this,"Date not in the Correct format. Unable to proceed", Toast.LENGTH_SHORT).show();
        }

        if(id.getText().toString().equals("")) {
            dataTransfer.open();
            Meeting m = dataTransfer.createMeeting(meeting);
            id.setText(""+m.getId());
            description.setText(m.getDescription());
            dataTransfer.close();
            Toast.makeText(this, "New Meeting has been created!",Toast.LENGTH_SHORT).show();
        }
        else{
            try {
                meeting.setId(Long.valueOf(id.getText().toString()));
            }catch(Exception e)
            {
                Toast.makeText(this,"ID not in the Correct format. Unable to proceed", Toast.LENGTH_SHORT).show();
            }
            dataTransfer.open();
            dataTransfer.editMeeting(meeting);
            dataTransfer.close();
            Toast.makeText(this, "Meeting has been updated!",Toast.LENGTH_SHORT).show();
        }

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}


