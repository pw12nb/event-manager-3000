package inventtech.eventmanager3000;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Outline;
import android.os.Build;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import inventtech.eventmanager3000.Adapters.MeetingListAdapter;
import inventtech.eventmanager3000.DTO.MeetingDTO;
import inventtech.eventmanager3000.Database.Meeting;

public class MainActivity extends AppCompatActivity {

    MeetingDTO dataTransfer;
    List<Meeting> todaysMeetings;
    CalendarView calendar;
    ListView meetingList;
    TextView noMeetings;
    TextView date;
    Button deleteAll;
    int g_month, g_day, g_year;
    String DAY_OF_MONTH = "day";
    String CALENDAR_MONTH = "month";
    String CALENDAR_YEAR = "year";

    /**
     * Create the new View
     * @param savedInstanceState the saved instance
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //initialize the needed variables
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        calendar = (CalendarView)findViewById(R.id.calendar);
        dataTransfer = new MeetingDTO(this);
        deleteAll = (Button)findViewById(R.id.delete_all);
        g_month = 0;
        g_day = 0;
        g_year = 0;
        meetingList = (ListView)findViewById(R.id.eventList);
        noMeetings = (TextView)findViewById(R.id.no_events);
        date = (TextView)findViewById(R.id.selected_date);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("EEEE, MMMM d, yyyy");
        date.setText(format.format(c.getTime()));

        //restore the saved state (the currently selected date) o/w initialize
        if (savedInstanceState != null) {
            // Restore value of members from saved state
            g_year = savedInstanceState.getInt(CALENDAR_YEAR);
            g_month = savedInstanceState.getInt(CALENDAR_MONTH);
            g_day = savedInstanceState.getInt(DAY_OF_MONTH);
        } else {
            Calendar newC = Calendar.getInstance();
            g_year = newC.get(newC.YEAR);
            g_month = newC.get(newC.MONTH);
            g_day = newC.get(newC.DAY_OF_MONTH);
        }

        //load the status bar
        Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(this.getResources().getColor(R.color.DarkTeal));

        setSupportActionBar(myToolbar);
        myToolbar.setTitleTextColor(getResources().getColor(R.color.white));

        //add the material design add button (the plus)
        View addButton = findViewById(R.id.add_button);
        addButton.setOutlineProvider(new ViewOutlineProvider() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void getOutline(View view, Outline outline) {
                int diameter = getResources().getDimensionPixelSize(R.dimen.diameter);
                outline.setOval(0, 0, diameter, diameter);
            }
        });
        addButton.setClipToOutline(true);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddNewMeeting();
            }
        });

        //load the list of meetings
        loadMeetingsForDate(g_year, g_month, g_day);

        //initialize the list adapter to display it on the screen
        MeetingListAdapter listAdapter = new MeetingListAdapter(this, R.layout.list_item_layout, todaysMeetings);
        meetingList.setAdapter(listAdapter);

        //update the meetings list if the date changes
        calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                g_year = year;
                g_month = month;
                g_day = dayOfMonth;
                loadMeetingsForDate(year, month, dayOfMonth);
            }
        });

        //move to the edit page if that meeting is selected
        meetingList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int position,
                                    long arg3) {
                Meeting meeting = ((MeetingListAdapter) adapter.getAdapter()).getItem(position);

                Intent intent = new Intent(getBaseContext(), AddMeeting.class);
                intent.putExtra("MEETING_DATA", meeting);
                startActivity(intent);
                // TODO Auto-generated method stub

            }

        });
    }

    /**
     * Delete all the meetings for the current date
     * @param v the view that was selected
     */
    public void DeleteAll(View v)
    {
        Calendar c = Calendar.getInstance();
        c.set(g_year, g_month, g_day, 0, 0);
        calendar.setDate(c.getTimeInMillis());
        dataTransfer.open();
        dataTransfer.DeleteAll(new Date(c.getTimeInMillis()));
        loadMeetingsForDate(g_year, g_month, g_day);
        dataTransfer.close();
    }

    /**
     * Save the current selected date to ensure it survives the rotation and recreation
     * @param savedInstanceState
     */
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the user's current game state
        savedInstanceState.putInt(DAY_OF_MONTH, g_day);
        savedInstanceState.putInt(CALENDAR_MONTH, g_month);
        savedInstanceState.putInt(CALENDAR_YEAR, g_year);

        // Always call the superclass
        super.onSaveInstanceState(savedInstanceState);
    }

    /**
     * Load all the meeting for the supplied date
     * @param year the year (e.g. 2015)
     * @param month the month (1-12)
     * @param dayOfMonth the day of the month (1-31)
     */
    public void loadMeetingsForDate(int year, int month, int dayOfMonth)
    {
        try {
            //use the DTO
            dataTransfer.open();
            Calendar c = Calendar.getInstance();
            c.set(year, month, dayOfMonth, 0, 0);
            calendar.setDate(c.getTimeInMillis());
            SimpleDateFormat format = new SimpleDateFormat("EEEE, MMMM d, yyyy");
            date.setText(format.format(c.getTime()));
            todaysMeetings = dataTransfer.getAllMeetings(new Date(c.getTimeInMillis()));
            MeetingListAdapter listAdapter = new MeetingListAdapter(getBaseContext(), R.layout.list_item_layout, todaysMeetings);
            meetingList.setAdapter(listAdapter);
            listAdapter.notifyDataSetChanged();
            if(todaysMeetings.isEmpty())            {
                noMeetings.setVisibility(View.VISIBLE);
                meetingList.setVisibility(View.GONE);
            }
            else{
                noMeetings.setVisibility(View.GONE);
                meetingList.setVisibility(View.VISIBLE);
            }
            dataTransfer.close();
        }catch(Exception e)
        {
            Toast.makeText(this, "Cannot Load Meeting info", Toast.LENGTH_SHORT).show();
            dataTransfer.close();
        }
    }

    /**
     * Move to the add new meeting page
     */
    public void AddNewMeeting()
    {
        Intent intent = new Intent(getBaseContext(), AddMeeting.class);
        Calendar c = Calendar.getInstance();
        c.set(g_year, g_month, g_day, 0, 0);
        intent.putExtra("DATE_SELECTED", new Date(c.getTimeInMillis()).toString());
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * If the today item has been selected, move back to today in the calendar
     * @param item the menu item selected
     * @return successful
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch(item.getItemId()){
            case R.id.action_today:
                Calendar c = Calendar.getInstance();
                calendar.setDate(c.getTimeInMillis());
                g_year = c.get(c.YEAR);
                g_month = c.get(c.MONTH);
                g_day = c.get(c.DAY_OF_MONTH);
                dataTransfer.open();
                todaysMeetings = dataTransfer.getAllMeetings(new Date(c.getTimeInMillis()));
                dataTransfer.close();
                MeetingListAdapter listAdapter = new MeetingListAdapter(this, R.layout.list_item_layout, todaysMeetings);
                meetingList.setAdapter(listAdapter);

                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
