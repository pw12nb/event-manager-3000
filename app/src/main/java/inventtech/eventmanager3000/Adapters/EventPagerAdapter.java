package inventtech.eventmanager3000.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import inventtech.eventmanager3000.Fragments.EventList;
import inventtech.eventmanager3000.Fragments.EventSchedule;

/**
 * Created by Peter on 11/10/2015.
 */
public class EventPagerAdapter extends FragmentPagerAdapter {

    public EventPagerAdapter(FragmentManager fm)
    {
        super(fm);
    }
    EventList eventList;
    EventSchedule eventSchedule;

    @Override
    public Fragment getItem(int index) {
        switch(index)
        {
            case 0:
                if(eventList == null)
                    eventList = new EventList();
                return eventList;
            case 1:
                if(eventSchedule == null)
                    eventSchedule = new EventSchedule();
                return eventSchedule;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Event List";
            case 1:
                return "Schedule";
            default:
                return "Dunno";
        }
    }
}
