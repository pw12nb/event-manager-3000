package inventtech.eventmanager3000.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import inventtech.eventmanager3000.Database.Meeting;
import inventtech.eventmanager3000.R;

/**
 * Created by Peter on 11/11/2015.
 *
 * The adapter used to display the meeting in the main list view
 */
public class MeetingListAdapter extends ArrayAdapter<Meeting> {

    List<Meeting> listData = null;

    /**
     * The constructor that takes the new list of items
     * @param context the context to write to
     * @param resource the resource
     * @param items the list of Meeting to display
     */
    public MeetingListAdapter(Context context, int resource, List<Meeting> items) {
        super(context, resource, items);
        listData = items;
    }

    /**
     * Returns the meeting at the specified position
     * @param position the position index
     * @return the meeting object
     */
    @Override
    public Meeting getItem(int position) {
        // TODO Auto-generated method stub
        return listData.get(position);
    }


    /**
     * Returns the view to display in the list
     * @param position the index of the message you want displayed
     * @param convertView the view to add to
     * @param parent the parent view group
     * @return the new view
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.list_item_layout, null);
        }

        Meeting p = getItem(position);

        if (p != null) {
            TextView tt1 = (TextView) v.findViewById(R.id.id);
            TextView tt3 = (TextView) v.findViewById(R.id.description);

            if (tt1 != null) {
                tt1.setText(""+p.getId());
            }

            if (tt3 != null) {
                tt3.setText(p.getDescription());
            }
        }

        return v;
    }
}
