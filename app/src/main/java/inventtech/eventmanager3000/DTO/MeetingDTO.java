package inventtech.eventmanager3000.DTO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import inventtech.eventmanager3000.Database.Meeting;
import inventtech.eventmanager3000.Helper.MeetingDatabaseHelper;

/**
 * Created by Peter on 9/12/2015.
 *
 * The Data Transfer Object that handles database interactions
 */
public class MeetingDTO {

    // Database fields
    private SQLiteDatabase database;
    private MeetingDatabaseHelper dbHelper;
    private String[] allColumns = { MeetingDatabaseHelper.COLUMN_ID,
                    MeetingDatabaseHelper.COLUMN_date,
                    MeetingDatabaseHelper.COLUMN_description,
                    MeetingDatabaseHelper.COLUMN_time,
                    MeetingDatabaseHelper.COLUMN_location,
                    MeetingDatabaseHelper.COLUMN_contact,
                    MeetingDatabaseHelper.COLUMN_notes
    };

    /**
     * Constructor
     * @param context the current context     */
    public MeetingDTO(Context context) {
        dbHelper = new MeetingDatabaseHelper(context);
    }

    /**
     * Open the connection to the database
     * @throws SQLException if connection was unsuccessful
     */
    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    /**
     * Close the connection to the database
     */
    public void close() {
        dbHelper.close();
    }

    /**
     * Create a new meeting based on the Meeting object passed in
     * @param meeting the new meeting object
     * @return the meeting object if it was added successfully
     */
    public Meeting createMeeting(Meeting meeting) {
        ContentValues values = new ContentValues();
        values.put(MeetingDatabaseHelper.COLUMN_date, meeting.getDateOfMeeting().toString());
        values.put(MeetingDatabaseHelper.COLUMN_description, meeting.getDescription());
        values.put(MeetingDatabaseHelper.COLUMN_time, meeting.getTime().toString());
        values.put(MeetingDatabaseHelper.COLUMN_location, meeting.getLocation());
        values.put(MeetingDatabaseHelper.COLUMN_contact, meeting.getContact());
        values.put(MeetingDatabaseHelper.COLUMN_notes, meeting.getNotes());
        long insertId = database.insert(MeetingDatabaseHelper.TABLE_MEETINGINFO, null,
                values);
        Cursor cursor = database.query(MeetingDatabaseHelper.TABLE_MEETINGINFO,
                allColumns, MeetingDatabaseHelper.COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Meeting newMeeting = null;
        if (!cursor.isAfterLast()) {
            newMeeting = cursorToMeeting(cursor);
        }
        cursor.close();
        return newMeeting;
    }

    /**
     * Edit the meeting with the supplied meeting object, uses the meeting ID
     * @param meeting the updated meeting object
     * @return the updated meeting object if successful
     */
    public Meeting editMeeting(Meeting meeting) {
        long id = meeting.getId();
        ContentValues values = new ContentValues();
        values.put(MeetingDatabaseHelper.COLUMN_date, meeting.getDateOfMeeting().toString());
        values.put(MeetingDatabaseHelper.COLUMN_description, meeting.getDescription());
        values.put(MeetingDatabaseHelper.COLUMN_time, meeting.getTime().toString());
        values.put(MeetingDatabaseHelper.COLUMN_location, meeting.getLocation());
        values.put(MeetingDatabaseHelper.COLUMN_contact, meeting.getContact());
        values.put(MeetingDatabaseHelper.COLUMN_notes, meeting.getNotes());
        System.out.println("Meeting updated with id: " + id);

        long updateId = database.update(MeetingDatabaseHelper.TABLE_MEETINGINFO, values, MeetingDatabaseHelper.COLUMN_ID + " = ?",
                new String[]{String.valueOf(meeting.getId())});
        Cursor cursor = database.query(MeetingDatabaseHelper.TABLE_MEETINGINFO,
                allColumns, MeetingDatabaseHelper.COLUMN_ID + " = " + updateId, null,
                null, null, null);
        cursor.moveToFirst();
        Meeting newMeeting = null;
        if (!cursor.isAfterLast()) {
            newMeeting = cursorToMeeting(cursor);
        }
        cursor.close();
        return newMeeting;
    }

    /**
     * Delete all meetings on a specified date
     * @param date
     */
    public void DeleteAll(Date date)
    {
        database.delete(MeetingDatabaseHelper.TABLE_MEETINGINFO,
                MeetingDatabaseHelper.COLUMN_date + " = ?", new String[]{date.toString()});
    }

    /**
     * Delay the meeting to the next weekday, or weekend depending on the day it lands on
     * @param meeting the meeting to edit
     * @param forward if forward or backward change
     * @return the newly updated meeting
     */
    public Meeting delayMeeting(Meeting meeting, boolean forward) {
        long id = meeting.getId();
        ContentValues values = new ContentValues();
        if(meeting.getDateOfMeeting() != null) {
            Calendar cal = Calendar.getInstance();
            //Update the date to the next week day or weekend day based on which of the two it
            //currently lands on
            cal.setTime(meeting.getDateOfMeeting());
            if(forward)
            {
                if(cal.get(cal.DAY_OF_WEEK) == cal.FRIDAY)
                    cal.add(cal.DATE, 3);
                else if(cal.get(cal.DAY_OF_WEEK) == cal.SUNDAY)
                    cal.add(cal.DATE, 6);
                else
                    cal.add(cal.DATE, 1);
            }
            else
            {
                if(cal.get(cal.DAY_OF_WEEK) == cal.MONDAY)
                    cal.add(cal.DATE, -3);
                else if(cal.get(cal.DAY_OF_WEEK) == cal.SATURDAY)
                    cal.add(cal.DATE, -6);
                else
                    cal.add(cal.DATE, -1);
            }
            values.put(MeetingDatabaseHelper.COLUMN_date, new Date(cal.getTimeInMillis()).toString());

            System.out.println("Meeting updated with id: " + id);
            //save the update
            long updateId = database.update(MeetingDatabaseHelper.TABLE_MEETINGINFO, values, MeetingDatabaseHelper.COLUMN_ID + " = ?",
                    new String[]{String.valueOf(meeting.getId())});
            Cursor cursor = database.query(MeetingDatabaseHelper.TABLE_MEETINGINFO,
                    allColumns, MeetingDatabaseHelper.COLUMN_ID + " = " + updateId, null,
                    null, null, null);
            cursor.moveToFirst();
            Meeting newMeeting = null;
            if (!cursor.isAfterLast()) {
                newMeeting = cursorToMeeting(cursor);
            }
            cursor.close();
            return newMeeting;
        }
        else return null;
    }

    /**
     * Load a meeting based on the id
     * @param id the id of the meeting to find
     * @return the meeting
     */
    public Meeting loadMeeting(long id)
    {
        Cursor cursor = database.query(MeetingDatabaseHelper.TABLE_MEETINGINFO,
                allColumns, MeetingDatabaseHelper.COLUMN_ID+ " = ?", new String[] { ""+id }, null, null, null);
        cursor.moveToFirst();
        Meeting newMeeting = null;
        if (!cursor.isAfterLast()) {
            newMeeting = cursorToMeeting(cursor);
        }
        cursor.close();
        return newMeeting;
    }

    /**
     * Delete the current meeting
     * @param meeting the meeting to delete
     */
    public void deleteMeeting(Meeting meeting) {
        long id = meeting.getId();
        System.out.println("Meeting deleted with id: " + id);
        database.delete(MeetingDatabaseHelper.TABLE_MEETINGINFO, MeetingDatabaseHelper.COLUMN_ID
                + " = " + id, null);
    }

    /**
     * Get all of the meetings for a certain date
     * @param date the date to grab for
     * @return a list of meetings on that date
     */
    public List<Meeting> getAllMeetings(Date date) {
        List<Meeting> schedule = new ArrayList<Meeting>();

        Cursor cursor = database.query(MeetingDatabaseHelper.TABLE_MEETINGINFO,
                allColumns, MeetingDatabaseHelper.COLUMN_date + " = ?", new String[] { date.toString() }, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Meeting meeting = cursorToMeeting(cursor);
            schedule.add(meeting);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return schedule;
    }

    /**
     * Convert the database return into a meeting object
     * @param cursor the cursor for the db results
     * @return the new meeting
     */
    private Meeting cursorToMeeting(Cursor cursor) {
        Meeting meeting = new Meeting();
        meeting.setId(cursor.getLong(0));
        meeting.setDateOfMeeting(Date.valueOf(cursor.getString(1)));
        meeting.setDescription(cursor.getString(2));
        meeting.setTime(cursor.getString(3));
        meeting.setLocation(cursor.getString(4));
        meeting.setContact(cursor.getString(5));
        meeting.setNotes(cursor.getString(6));
        return meeting;
    }
}
